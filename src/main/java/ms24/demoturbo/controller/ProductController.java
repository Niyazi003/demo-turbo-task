package ms24.demoturbo.controller;

import lombok.RequiredArgsConstructor;
import ms24.demoturbo.dto.ProductResponseDto;
import ms24.demoturbo.dto.ProductRequestDto;
import ms24.demoturbo.entity.ProductEntity;
import ms24.demoturbo.entity.UserEntity;
import ms24.demoturbo.services.ProductServices;
import ms24.demoturbo.services.ProductServicesImpl;
import ms24.demoturbo.services.UserServices;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {
    private final ProductServices productServices;
    private final UserServices userServices;

    @PostMapping("/{id}")
    public ProductResponseDto createProduct(@PathVariable Long id, @RequestBody ProductRequestDto prouctRequestDto){
        return productServices.createProduct(id,prouctRequestDto);
    }

    @GetMapping("/{id}")
    public ProductResponseDto getProductById(@PathVariable Long id){
        return productServices.getProductById(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable Long id){
        productServices.deleteProduct(id);
    }

}
