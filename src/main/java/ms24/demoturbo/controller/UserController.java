package ms24.demoturbo.controller;

import lombok.RequiredArgsConstructor;
import ms24.demoturbo.dto.UserRequestDto;
import ms24.demoturbo.dto.UserResponseDto;
import ms24.demoturbo.services.UserServices;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {
    private final UserServices userServices;
    @GetMapping("/{id}")
    public UserResponseDto getUserById(@PathVariable Long id){
        return userServices.findUserById(id);
    }

    @PostMapping("/create-user")
   public UserResponseDto createUser(@RequestBody UserRequestDto userRequestDto){
        return userServices.createUser(userRequestDto);
    }

    @GetMapping
   public Page<UserResponseDto> getAllUsers(Pageable pageable) {
        return userServices.getAllUsers(pageable);
    }
    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable Long id){
        userServices.deleteUser(id);
    }
    @PutMapping("/{id}")
    public UserResponseDto updateUser(@PathVariable Long id,@RequestBody UserRequestDto userRequestDto){
        return userServices.updateUser(id,userRequestDto);
    }

}
