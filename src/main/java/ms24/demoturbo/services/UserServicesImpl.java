package ms24.demoturbo.services;

import lombok.RequiredArgsConstructor;
import lombok.ToString;
import ms24.demoturbo.dto.ProductResponseDto;
import ms24.demoturbo.dto.UserResponseDto;
import ms24.demoturbo.dto.UserRequestDto;
import ms24.demoturbo.entity.ProductEntity;
import ms24.demoturbo.entity.UserEntity;
import ms24.demoturbo.repo.UserRepo;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.lang.System.out;

@Service
@RequiredArgsConstructor

public class UserServicesImpl implements UserServices{
    private final UserRepo userRepo;
    private final ModelMapper modelMapper;

    @Override
    public UserResponseDto findUserById(Long id) {
       UserEntity userEntity= userRepo.findById(id).orElseThrow();
        List<ProductResponseDto> productResponseDto = userEntity.getProductEntity()
                .stream()
                .map(productEntity -> modelMapper.map(productEntity, ProductResponseDto.class))
                .collect(Collectors.toList());

        return UserResponseDto
               .builder()
               .id(userEntity.getId())
               .name(userEntity.getName())
               .phoneNumber(userEntity.getPhoneNumber())
                .productResponseDto(productResponseDto)
                .build();
    }


    @Override
    public Page<UserResponseDto> getAllUsers( Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(1, 10);
        return userRepo.findAll(pageRequest).map(userEntity -> modelMapper.map(userEntity,UserResponseDto.class));
    }

    @Override
    public UserResponseDto createUser(UserRequestDto userRequestDto) {
        UserEntity userEntity=UserEntity
                .builder()
                .name(userRequestDto.getName())
                .phoneNumber(userRequestDto.getPhoneNumber())
                .build();
        userRepo.save(userEntity);
        return UserResponseDto
                .builder()
                .id(userEntity.getId())
                .name(userEntity.getName())
                .phoneNumber(userEntity.getPhoneNumber())
                .build();
    }

    @Override
    public void deleteUser(Long id) {
        userRepo.deleteById(id);

    }

    @Override
    public UserResponseDto updateUser(Long id, UserRequestDto userRequestDto) {
       UserEntity userEntity= userRepo.findById(id).orElseThrow();
       UserEntity userResponseDto=UserEntity
               .builder()
               .id(userEntity.getId())
               .name(userRequestDto.getName())
               .phoneNumber(userRequestDto.getPhoneNumber())
               .build();
       userRepo.save(userResponseDto);

         return modelMapper.map(userResponseDto,UserResponseDto.class);

    }

    @Override
    public Page<UserResponseDto> list(UserRequestDto searchDto, Pageable page) {
        return null;
    }
}
