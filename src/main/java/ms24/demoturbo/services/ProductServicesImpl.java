package ms24.demoturbo.services;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import ms24.demoturbo.dto.ProductResponseDto;
import ms24.demoturbo.dto.ProductRequestDto;
import ms24.demoturbo.entity.ProductEntity;
import ms24.demoturbo.entity.UserEntity;
import ms24.demoturbo.repo.ProductRepo;
import ms24.demoturbo.repo.UserRepo;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductServicesImpl implements ProductServices{
    private final UserRepo userRepo;
    private final ProductRepo productRepo;
    private final ModelMapper modelMapper;


    @Override
    public ProductResponseDto createProduct(Long id, ProductRequestDto productRequestDto) {
//        UserEntity user = userRepo.findById(id).orElseThrow();
//        ProductEntity productEntity=ProductEntity
//                .builder()
//                .imgUrl(productRequestDto.getImgUrl())
//                .advertInsertTime(productRequestDto.getAdvertInsertTime())
//                .userEntity(user)
//                .build();
//        user.setProductEntity(List.of(productEntity));
//        productRepo.save(productEntity);
//        return ProductResponseDto
//                .builder()
//                .id(productEntity.getId())
//                .imgUrl(productEntity.getImgUrl())
//                .advertInsertTime(productEntity.getAdvertInsertTime())
//                .build();
        UserEntity user = userRepo.findById(id).orElseThrow();

        ProductEntity productEntity = modelMapper.map(productRequestDto, ProductEntity.class);
        productEntity.setUserEntity(user);

        productEntity = productRepo.save(productEntity);

        return modelMapper.map(productEntity, ProductResponseDto.class);
    }

    @Override
    public ProductResponseDto getProductById(Long id) {
      ProductEntity productEntity=productRepo.findById(id).orElseThrow();
        UserEntity userEntity = productEntity.getUserEntity();

        return ProductResponseDto
                .builder()
                .id(productEntity.getId())
                .imgUrl(productEntity.getImgUrl())
                .advertInsertTime(productEntity.getAdvertInsertTime())
                .userDtoName(userEntity.getName()+" "+userEntity.getId())
                .build();
    }

    @Override
    public ProductResponseDto updateProduct(Long id, ProductRequestDto productRequestDto) {
        ProductEntity byId = productRepo.findById(id).orElseThrow();

        return null;
    }

    @Override
    public void deleteProduct(Long id) {
        productRepo.deleteById(id);
    }

    @Override
    public Page<ProductResponseDto> list(ProductResponseDto productResponseDto, Pageable page) {
        productRepo.findAll((Specification<ProductEntity>) (root, query, criteriaBuilder) -> {
            final Predicate[] specification = specification( productResponseDto,root, criteriaBuilder);
            return criteriaBuilder.and(specification);
        });


        return null;
    }

    private Predicate[] specification(ProductResponseDto productResponseDto, Root<ProductEntity> root, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicateList = new ArrayList<>();

        if (productResponseDto.getImgUrl() != null) {
            final Predicate predicate = criteriaBuilder.equal(root.get(ProductEntity.Fields.imgUrl), productResponseDto.getImgUrl());
            predicateList.add(predicate);
        }


        if (productResponseDto.getAdvertInsertTime() != null) {
            final Predicate predicate = criteriaBuilder.equal(root.get(ProductEntity.Fields.advertInsertTime), productResponseDto.getAdvertInsertTime());
            predicateList.add(predicate);
        }



        return predicateList.toArray(new Predicate[0]);

    }


}
