package ms24.demoturbo.services;

import ms24.demoturbo.dto.ProductResponseDto;
import ms24.demoturbo.dto.ProductRequestDto;
import ms24.demoturbo.entity.ProductEntity;
import ms24.demoturbo.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;

public interface ProductServices {
    ProductResponseDto createProduct(Long id, ProductRequestDto prouctRequestDto);
    ProductResponseDto getProductById(Long id);
    ProductResponseDto updateProduct(Long id, ProductRequestDto prouctRequestDto);
    void deleteProduct(Long id);

   // List<ProductEntity> findProducts(Long id, String imgUrl, LocalDate advertInsertTime);
   Page<ProductResponseDto> list(ProductResponseDto searchDto, Pageable page);

}
