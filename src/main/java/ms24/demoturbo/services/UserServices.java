package ms24.demoturbo.services;

import ms24.demoturbo.dto.UserResponseDto;
import ms24.demoturbo.dto.UserRequestDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface UserServices {
    UserResponseDto findUserById(Long id);
    Page<UserResponseDto> getAllUsers(Pageable pageable);
    UserResponseDto createUser(UserRequestDto userRequestDto);
    void deleteUser(Long id);
    UserResponseDto updateUser(Long id, UserRequestDto userRequestDto);
    Page<UserResponseDto> list(UserRequestDto searchDto, Pageable page);
}
