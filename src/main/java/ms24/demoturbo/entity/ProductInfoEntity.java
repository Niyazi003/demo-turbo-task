package ms24.demoturbo.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import lombok.experimental.FieldNameConstants;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
@FieldDefaults(level = AccessLevel.PRIVATE)
@ToString(exclude = {"productEntity"})
public class ProductInfoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    Long productionYear;
    String banType;
    String colour;
    Boolean engine;
    Boolean newOr;
    @OneToOne
    ProductEntity productEntity;
}
