package ms24.demoturbo.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import lombok.experimental.FieldNameConstants;

import java.time.LocalDate;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
@FieldDefaults(level = AccessLevel.PRIVATE)
@ToString(exclude = {"userEntity"})
public class ProductEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String imgUrl;
    @JsonFormat(pattern = "yyyy-MM-dd")
    LocalDate advertInsertTime;
    @ManyToOne
    UserEntity userEntity;
    @OneToOne(mappedBy = ProductInfoEntity.Fields.productEntity, cascade = CascadeType.ALL)
    ProductInfoEntity productInfoEntity;

}
