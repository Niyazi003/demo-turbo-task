package ms24.demoturbo.dto;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProductResponseInfoDto {
    Long id;
    Long productionYear;
    String banType;
    String colour;
    Boolean engine;
    Boolean newOr;
    UserResponseDto userDto;
    ProductResponseDto productDto;

}
