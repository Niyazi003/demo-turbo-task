package ms24.demoturbo.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProductRequestInfoDto {
    Long productionYear;
    String banType;
    String colour;
    Boolean engine;
    Boolean newOr;
    UserResponseDto userDto;
    ProductResponseDto productDto;
}
