package ms24.demoturbo.dto;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserResponseDto {
    Long id;
    String name;
    String phoneNumber;
    List<ProductResponseDto> productResponseDto;


}
