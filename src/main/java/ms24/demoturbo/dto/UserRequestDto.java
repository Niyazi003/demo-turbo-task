package ms24.demoturbo.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;
import ms24.demoturbo.entity.ProductEntity;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserRequestDto {
    String name;
    String phoneNumber;
    List<ProductResponseDto> productResponseDto;
}
