package ms24.demoturbo.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProductRequestDto {
    String imgUrl;
    @JsonFormat(pattern="yyyy-MM-dd")
    LocalDate advertInsertTime;
    ProductResponseInfoDto productInfoDto;
 //   UserResponseDto userDto;
}
