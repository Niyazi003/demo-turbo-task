package ms24.demoturbo.repo;

import ms24.demoturbo.entity.ProductInfoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductInfoRepo extends JpaRepository<ProductInfoEntity,Long> {
}
