package ms24.demoturbo.repo;

import ms24.demoturbo.entity.ProductEntity;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepo extends JpaRepository<ProductEntity,Long> {
    List<ProductEntity> findAll(Specification<ProductEntity> specification);
}
